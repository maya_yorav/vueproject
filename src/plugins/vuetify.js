import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        primary: '#0097d5',
        secondary: '#3f51b5',
        accent: '#607d8b',
        error: '#f44336',
        warning: '#e91e63',
        info: '#cddc39',
        success: '#009688',
        background: 'blue-grey lighten-3'
    },
})