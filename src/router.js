import Vue from 'vue'
import Router from 'vue-router'
import NewBet from './views/NewBet.vue'
import BetsList from './views/BetsList.vue'
import Contacts from './views/Contacts.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/NewBet',
      component: NewBet
    },
    {
      path: '/',
      component: BetsList
    },
    {
      path: '/Contacts',
      component: Contacts
    }
  ]
})
