import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bets:
        [
            {
                title: "my bet title",
                bet_cost: "30 $",
                description: "We were in the park bla bla",
                user1: {
                    name: "Lee",
                    gender: "male"
                },
                user2: {
                    name: "Oran",
                    gender: "male"
                }
            },
            {
                title: "my bet title 2",
                bet_cost: "30 $",
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis, eros sit amet consequat semper, lacus mauris dignissim lorem, s. dimentum, bibendum eros porttitor, dignissim libero. ",
                user1: {
                    name: "Maya",
                    gender: "female"
                },
                user2: {
                    name: "Oran",
                    gender: "male"
                }
            },
        ]
  },
  mutations: {

  },
  actions: {

  }
})
